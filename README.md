# PODAS 2023 - Columnar Exercise

## Setting up on NAF

Login to naf with your username:

    ssh username@naf-cms.desy.de

Then execute the following commands to install a local jupyter kernel that we will use forthgoing:

    source /nfs/dust/cms/user/bachjoer/podas_env/bin/activate 
    python -m ipykernel install --user --name=podas

Finally, clone this repository into your home directory (or wherever you want):

    git clone https://gitlab.cern.ch/cms-podas23/topical/columnar-exercise.git

You can then go to https://naf-jhub.desy.de/ and login to get a Jupyter notebook.

## Exercise

An introduction to columnar programming is found within `columnar-exercise.ipynb`. It is recommended to go through this notebook first.

After that, we will do a hands-on example involving the decay of the Higgs boson H -> ZZ -> 4l, which can be found in `HZZ4l.ipynb`.

