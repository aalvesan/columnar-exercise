{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c1a866ac-51e2-4047-8476-7761ab171038",
   "metadata": {},
   "source": [
    "## PODAS 2023\n",
    "# Topical exercise: Columnar analysis\n",
    "\n",
    "This exercise is intended to give a very short introduction to columnar programming in python with the packages `awkward` and `coffea`, as well as to working with the CMS `nanoAOD` event format."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd01c33a-7298-455f-94d8-fb32f5dba07a",
   "metadata": {},
   "source": [
    "### Part 1: Reading a nanoAOD file\n",
    "\n",
    "In this first part, we will read a CMS nanoAOD file, inspect its contents, and make a plot of the contained data.\n",
    "The exercise is adapted from https://coffeateam.github.io/coffea/notebooks/nanoevents.html.\n",
    "\n",
    "We use the package coffea to read nanoAOD files. This package already implements many behaviours of the corresponding physics objects, such as Lorentz vectors, matching between jets and particles, and more, making it very convenient.\n",
    "\n",
    "To start, read events from a file like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "742d4f7a-bab2-48fb-afd9-8576b1a795a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import awkward as ak\n",
    "from coffea.nanoevents import NanoEventsFactory, NanoAODSchema\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "877bc878-e84b-4eb4-8f8d-ab2ec486fbf4",
   "metadata": {},
   "outputs": [],
   "source": [
    "fname = '/nfs/dust/cms/group/cmsdas2023/columnar-exercise/DY.root'\n",
    "# In case you do not have access to NAF, there is also a much smaller dataset included in the git repo:\n",
    "#fname = 'dy.root'\n",
    "events = NanoEventsFactory.from_root(fname).events()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd9e98c2-5337-4436-ba9e-58bfbcc8cd43",
   "metadata": {},
   "source": [
    "`events` now is a container object containing the different branches (i.e. physics objects) in the nanoAOD file. Note that coffea by default uses lazy loading - a branch is only fully loaded to memory when we try to access it.\n",
    "\n",
    "We can print the available branches in the file like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4effbce-5ba9-43c6-b8cd-9b620459c4af",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.fields"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4fb4b97-68fd-4d56-aace-cb691c9e49c7",
   "metadata": {},
   "source": [
    "To access a branch - for example, jets - we simply write:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7750fc8-59b4-475e-a9db-9940191e60bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.Jet"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed2dcec6-b607-4089-aa3c-6d8ccfc75417",
   "metadata": {},
   "source": [
    "Note the type of the `events.Jet` object: it is a 2D array of `Jet` with length `2000 * var`. `2000` here is the number of total events in the file. However, not all events will have the same number of jets! Thus, in columnar analysis, we need to work with `var`iable-length arrays (also called jagged arrays)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7676f759-f3ac-452d-b181-6ff46bb06517",
   "metadata": {},
   "source": [
    "Most of the branches in a nanoAOD file have sub-branches describing properties of the correspoinding physics objects. \n",
    "For example, a jet has its four momentum - expressed in terms of `pt`, `eta`, `phi` and `mass` - but also many other variables describing its reconstruction, constituents, identification criteria and much more:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b29cf173-ce32-45b1-af56-f94de8c43589",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.Jet.fields"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d49c0530-9fdc-430b-9516-24fc2b1560bd",
   "metadata": {},
   "source": [
    "nanoAOD is self-documenting - each branch and sub-branch (at least in theory) contains a short explanation of what the branch refers to. We can read it like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b7c10450-190a-4dd4-b5a4-91b140deb847",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.Jet.nConstituents.__doc__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2325853-1101-408e-89ce-6ffb96b7e83c",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.Jet.btagDeepB.__doc__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0348ee3f-5623-43c2-91b1-2e5e40fea47d",
   "metadata": {},
   "source": [
    "A full overview of the branches in nanoAOD can also be found online [here](https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv9/2018UL/doc_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8_RunIISummer20UL18NanoAODv9-106X_upgrade2018_realistic_v16_L1v1-v1.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a155a3cb-890e-4eec-b294-f96d17041e3a",
   "metadata": {},
   "source": [
    "## Part 2 Awkward Array and Jaggedness\n",
    "\n",
    "Let us now actually look at the content of a branch. For a first simple example, we will use the missing transverse energy (MET):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d2ad76b-06c3-42a4-8884-ed1636aa07a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.MET"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fcc81e63-336f-4a06-b102-52f62ecfc697",
   "metadata": {},
   "source": [
    "There is only one entry of `MET` for each event, so we do not have to worry about jagged arrays for now. We can directly access e.g. the `pt` (i.e. the magnitude) of the missing transverse energy like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3fc32f6d-a51e-4766-b71e-709d2992f737",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.MET.pt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1767a423-3c84-4afb-b292-dfc472277146",
   "metadata": {},
   "source": [
    "This is now a simple array of floats, and we can treat it like we would in standard python with `numpy`. For example, we can make a simple plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dfbe6822-7024-4ae6-84ee-2cbbea6790a3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.hist(events.MET.pt, bins=100, range=(0, 200))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77148b39-5b6f-4416-af23-881dfecfcf37",
   "metadata": {},
   "source": [
    "Note that the `MET` object also implements `FourVector` mixins (see [here](https://coffeateam.github.io/coffea/modules/coffea.nanoevents.methods.vector.html)) and can thus be used for Lorentz vector computations. For example, we can get the cartesian x and y components of the MET by simply calling "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e7af095-191b-425e-ac84-c03ea464ecd6",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.MET.x"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2ec325f",
   "metadata": {},
   "source": [
    "Let us now have a look at jets. We expect that different events have a different number of jets, how can we work with that?  As already mentioned, a feature called 'jagged arrays' which are part of awkward allow us to write 'intuitive' code with these arrays. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b128bd8d",
   "metadata": {},
   "source": [
    "Let us now have a look at jets.  Let's see if we can select the first jet for each event. We would expect to, again, get an array of 2000 `Jet` objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "802c9475",
   "metadata": {},
   "outputs": [],
   "source": [
    "#get the first jet \n",
    "events.Jet[:,0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5020afb",
   "metadata": {},
   "source": [
    "Oops.\n",
    "\n",
    "So what happened here?\n",
    "\n",
    "The problem is that not all events will have the same number of jets - especially, there might be some events that do not have jets at all. Since we were trying to get the first jet of all events, we got an error, because for some events this jet does not exist."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bfb44b0-f990-4c74-8332-7bd2bad271ba",
   "metadata": {},
   "source": [
    "In general, we call arrays with a variable number of entries per event jagged arrays. The package `awkward` is used to deal with such quantities. In many situations, treating them requires slightly more care than simple fixed-length arrays as we are used to from `numpy`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dcb27a0a",
   "metadata": {},
   "source": [
    "One way on how to deal with this is to filter our array before such operations to make sure that we only operate on the events with enough entries. \n",
    "\n",
    "##  Part 3 : Applying simple Cuts / Filtering Arrays\n",
    "\n",
    "Similarly as in numpy, we can use boolean expressions in array slicing to select the subarray that fulfills the conditions that we have. \n",
    "\n",
    "For our jet example, we can select the events that have at least 1 jet by using the `ak.num` method. This method returns the length of an awkward array at a specified level:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a88785e5-1d51-4a6f-9760-30c486352f70",
   "metadata": {},
   "outputs": [],
   "source": [
    "n_jet = ak.num(events.Jet)\n",
    "n_jet"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3a0536d-b083-4e2f-b330-3ce1f4bd9165",
   "metadata": {},
   "source": [
    "Now, lets try to select events that have at least one jet:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ecdd9a1f",
   "metadata": {},
   "outputs": [],
   "source": [
    "events_onejet = events[n_jet >= 1]\n",
    "events_onejet"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "395c5eb1-ff42-418f-8d04-6f8bb2a1db0e",
   "metadata": {},
   "source": [
    "It is important to note that we have now cut away some of the events. When doing this, we have to keep track which events we sorted out - otherwise we might get some nasty bugs later on.\n",
    "\n",
    "Let's have a look at the amount of events that pass our 'at least one jet' cut and calculate the efficiency. We can use the python built in `len()` method for this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c3921b4",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Original number of events: \", len(events))\n",
    "print(\"Number of events after requiring at least one jet: \", len(events_onejet))\n",
    "efficiency = len(events_onejet)/len(events)\n",
    "print('The selection efficiency after this cut is  {:.2f}%'.format(efficiency*100))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0bfd3a9b-0fc8-4d23-ba16-574636008db0",
   "metadata": {},
   "source": [
    "Regardless, we can now get the first jet in each event and plot e.g. its transverse momentum:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "605954d6-5a07-4a9b-ae38-dd669906bb46",
   "metadata": {},
   "outputs": [],
   "source": [
    "first_jet = events_onejet.Jet[:,0]\n",
    "\n",
    "plt.hist(first_jet.pt, bins=100, range=(0, 200))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "497ec2b5-48c0-43c5-8ed4-b4a0a331ee9a",
   "metadata": {},
   "source": [
    "Note that we could have done this alternatively in one line like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b8e429a-147b-4f55-acf9-c4eb483004c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "first_jet = events.Jet[ak.num(events.Jet) >=1][:,0]\n",
    "\n",
    "# Same jets as before\n",
    "plt.hist(first_jet.pt, bins=100, range=(0, 200))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1393d5e",
   "metadata": {},
   "source": [
    "Similar to numpys implementation of `np.where`, which allows for more sophisticated implementations of the just shown filtering, awkward has a function called `ak.where`. \n",
    "Calling it with one argument:\n",
    "\n",
    "        ak.where(X>1)           returns the indices of X where X is greater than 1\n",
    "\n",
    "Calling it with 3 arguments:\n",
    "\n",
    "        ak.where(X>1, X, Y)     returns X is conditions is met and Y otherwise, does this \n",
    "        \n",
    "\n",
    "This is very useful if one needs to keep track of the events that pass the cut, because we can save the indices. \n",
    "        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cc03eee6",
   "metadata": {},
   "outputs": [],
   "source": [
    "indices_onejet = ak.where(ak.num(events.Jet)>=1)\n",
    "indices_onejet"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1e23a72-f2be-4827-bcef-cbb515e8fe38",
   "metadata": {},
   "outputs": [],
   "source": [
    "first_jet = events.Jet[indices_onejet][:,0]\n",
    "\n",
    "# Still, the same jets as before\n",
    "plt.hist(first_jet.pt, bins=100, range=(0, 200))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0fb9b2dd-acdf-4870-875b-fadd82054589",
   "metadata": {},
   "source": [
    "Again, we can perform mathematical operations on the four momenta of the `Jet` (or any other) objects. For example, we could select at least two jets, and add those two jet together to form the invariant mass:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "afae6db0-2378-4ccd-b30a-5df4ca4b12e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "events_twojets = events[ak.num(events.Jet)>=2]\n",
    "first_jet = events_twojets.Jet[:,0]\n",
    "second_jet = events_twojets.Jet[:,1]\n",
    "\n",
    "dijet = first_jet + second_jet\n",
    "dijet # This is now an array of Lorentz vectors - note that the Jet-specific information got dropped"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "65dcb407-c66c-4dee-93ac-a0c82502e032",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(dijet.mass, bins=100, range=(0, 300))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c4c334c",
   "metadata": {
    "tags": [
     "parameters"
    ]
   },
   "source": [
    "## Part 4: Histogramming and Plotting\n",
    "\n",
    "The workhorse of HEP analysis is often the histogram. Since detector experiments at colliders are essentially a 'sophisticated counting experiment', histograms offer a good way to manipulate and visualize data. \n",
    "The tools we have explored so far are all integrated into the `scikit-hep` framework for python. As such, they are tightly interwoven with the tool `hist` that allows for advanced yet intutive histogram manipulation. \n",
    "\n",
    "Further information can be found [here](https://github.com/scikit-hep/hist)\n",
    "\n",
    "See what we can do with one line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f322a084",
   "metadata": {},
   "outputs": [],
   "source": [
    "import hist\n",
    "hist.Hist.new.Regular(50, 0, 100, name='FirstJetPt', label='pt of the first jet').Double().fill((first_jet.pt)).plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a35e92cc",
   "metadata": {},
   "source": [
    "Let's disentangle what happened here: We have a histogram of the first jet pt, but how did that happen? \n",
    "First, we are creating a new histogram with axis type `Regular` - meaning equally-spaced bin edges - and giving it the start, stop, and number of bins:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9cacb34",
   "metadata": {},
   "outputs": [],
   "source": [
    "our_hist = hist.Hist.new.Regular(\n",
    "    50,                         # number of bins\n",
    "    0,                          # first edge of the first bin\n",
    "    100,                        # last edge of the last bin\n",
    "    name='FirstJetPt',          # we can give a name to the axis - useful for histograms with multiple dimensions\n",
    "    label='pt of the first jet' # an axis label that will appear on the plot\n",
    ").Double()     # We call Double() to signify that this histogram will store floating-point values\n",
    "\n",
    "#this has created a hist, let's see how it looks like just after initialization\n",
    "print(our_hist)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e098528",
   "metadata": {},
   "source": [
    "What a neat presentation.\n",
    "\n",
    "We can see that we have 50 bins evenly spaced between 0 and 100. The first and the last bins are overflow bins.\n",
    "\n",
    "Let's fill this histogram. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12ea00da",
   "metadata": {},
   "outputs": [],
   "source": [
    "#we can now fill our histogram by name\n",
    "our_hist.fill(FirstJetPt=first_jet.pt)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1534881",
   "metadata": {},
   "source": [
    "We could also create 2 dimensional histograms this way. `hist` offers a lot of features for projections, profiling and the like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6859066e",
   "metadata": {},
   "outputs": [],
   "source": [
    "h2 = hist.Hist.new.Regular(50, 0, 100, name=\"electronpt\").Regular(50, 0, 100, name=\"MET\").Double()\n",
    "h2.fill(electronpt=events.Electron[ak.num(events.Electron)>0][:,0].pt, MET=events.MET[ak.num(events.Electron)>0].pt)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "57341db4",
   "metadata": {},
   "source": [
    "Note that we again hat to make sure that we select events which have at least one electron. For a 2d histogram we of course also need to make sure that the events we fill in have the same amount of entries in each axis. \n",
    "\n",
    "\n",
    "### Plotting \n",
    "\n",
    "After we have made histograms, potentially applied all sorts of scale factors and the like, we of course need to make a shiny plot for a paper!  \n",
    "We use the mplhep library to make our basic matplotlib plots look like a root plot (because that's what people do...). We can also adapt CMS plotting standards!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97d8ab3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import mplhep as hep\n",
    "hep.style.set_style(\"CMS\")\n",
    "hep.cms.label(llabel='Training', data=False, lumi=50, year=2017)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54968af2",
   "metadata": {},
   "outputs": [],
   "source": [
    "#this figure is empty, let's plot the first jet pt from the histogram  before. Hist and mplhep work together, so we can easily pass the hist from before to mplheps histplot method\n",
    "\n",
    "hep.cms.label(llabel='Training', data=False, lumi=50, year=2017)\n",
    "hep.histplot(our_hist)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3711a1ce",
   "metadata": {},
   "source": [
    "Awesome! We are ready to get our hands dirty in a first real world example"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d05b56ad",
   "metadata": {},
   "source": [
    "## Bonus\n",
    "\n",
    "### Combinatorics in Awkward \n",
    "Keeping track of cuts can be hard and is certainly one of the major sources for errors in columnar analysis. \n",
    "Awkward gives us two tools which help us to get our combinatorics right:\n",
    "\n",
    "    1. ak.cartesian : Computes a Cartesian product (i.e. cross product) of data from a set of arrays. This operation creates records (if arrays is a dict) or tuples (if arrays is another kind of iterable) that hold the combinations of elements, and it can introduce new levels of nesting.\n",
    "\n",
    "    2. ak.combinations  :   Computes a Cartesian product (i.e. cross product) of array with itself that is restricted to combinations sampled without replacement. If the normal Cartesian product is thought of as an n dimensional tensor, these represent the “upper triangle” of sets without repetition. If replacement=True, the diagonal of this “upper triangle” is included.\n",
    "\n",
    "This is copied from the awkward documentation... Not very descriptive, we better have a look at an example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb01f2d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "one = ak.Array([1, 2, 3])\n",
    "two = ak.Array([\"a\", \"b\"])\n",
    "print(ak.cartesian([one, two], axis=0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7bfe5c60",
   "metadata": {},
   "outputs": [],
   "source": [
    "combi = ak.combinations(one, 2, axis=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c72344f",
   "metadata": {},
   "source": [
    "Something very useful happens when we `unzip` these: \n",
    "We can now get the left and right parts of these combinations as their own arrays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5dd90eeb",
   "metadata": {},
   "outputs": [],
   "source": [
    "lefts, rights = ak.unzip(combi)\n",
    "print(f'the combinations: {combi}')\n",
    "print(f'the left part: {lefts}')\n",
    "print(f'and the right part {rights}')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "482a0ed4",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "We can use this for example to reconstruct muon pairs.\n",
    "\n",
    "There are also methods that return this result in terms of their indices (ak.argcartesian and ak.argcombinations), which can be helpful in special cases."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d4a8dec",
   "metadata": {},
   "source": [
    "## Bonus 2: Interfacing to other Libraries\n",
    "\n",
    "It may sometimes be necessary to interface your data to other python standards. ML libraries often come with their own data array / tensor format (think tensorflow or pytorch) which include their own gimmicks for differentiation, gpu acceleration, etc. Also pandas has a very useful syntax to interact with tabular style data but required pandas dataframes to do so. \n",
    "\n",
    "These standards are often more restrictive in the sense that they usually don't allow for jaggedness. Especially in ML applications such as DNNs, often a certain shape of network input is required (for example a strict requirement of 3 jets per event). \n",
    "We'll see how to work with those down below. But at first we should start with a very basic option that awkward allows us to use: to_numpy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4d9147e",
   "metadata": {},
   "outputs": [],
   "source": [
    "#build a random awkward array\n",
    "inp_array = np.array([1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9])\n",
    "ak_array = ak.Array(inp_array)\n",
    "print(ak_array.type)\n",
    "print(ak_array)\n",
    "\n",
    "# call to array on this and see that it actually returns a numpy array\n",
    "rec_array = ak.to_numpy(ak_array)\n",
    "print(rec_array)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c052fc7",
   "metadata": {},
   "source": [
    "A very similar interface is available for pandas dataframes: ak.to_dataframe or ak.to_pandas depending on the awkward version you are using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53000d71",
   "metadata": {},
   "outputs": [],
   "source": [
    "ak_array_fromdict = ak.Array(\n",
    "    [\n",
    "        {\"x\": 1.1, \"y\": 1, \"z\": \"one\"},\n",
    "        {\"x\": 2.2, \"y\": 2, \"z\": \"two\"},\n",
    "        {\"x\": 3.3, \"y\": 3, \"z\": \"three\"},\n",
    "        {\"x\": 4.4, \"y\": 4, \"z\": \"four\"},\n",
    "        {\"x\": 5.5, \"y\": 5, \"z\": \"five\"},\n",
    "    ]\n",
    ")\n",
    "ak_array_fromdict"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "47bef957",
   "metadata": {},
   "outputs": [],
   "source": [
    "ak.to_pandas(ak_array_fromdict)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61c5b950",
   "metadata": {},
   "source": [
    "This function has some flexibility. It can deal with nested arrays: By default it uses pandas multiindices. \n",
    "Even jaggedness can be dealt with to some extent. By default it then returns a list of pandas dataframes. \n",
    "The option \"how\" allows to steer the behavior here:\n",
    "\n",
    " the mode \"inner\" preserves only subentries that exist for all nested dimensions (and thus may delete lines which do not exist along all dimensions). \n",
    "\n",
    " the mode \"outer\" preserves all subentries and thus adds extra entries where entries are \"missing\" in the awkward array, they are set to NAN\n",
    "\n",
    "\n",
    " This is quite hard to wrap your head around, so have a look at an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48aabb5b",
   "metadata": {},
   "outputs": [],
   "source": [
    "#build a badly jagged array\n",
    "jagged_array = ak.Array(\n",
    "    [\n",
    "        {\"x\": [], \"y\": [4.4, 3.3, 2.2, 1.1]},\n",
    "        {\"x\": [1], \"y\": [3.3, 2.2, 1.1]},\n",
    "        {\"x\": [1, 2], \"y\": [2.2, 1.1]},\n",
    "        {\"x\": [1, 2, 3], \"y\": [1.1]},\n",
    "        {\"x\": [1, 2, 3, 4], \"y\": []},\n",
    "    ]\n",
    ")\n",
    "jagged_array"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21683ae3",
   "metadata": {},
   "source": [
    "This is two pandas dataframes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08c5f719",
   "metadata": {},
   "outputs": [],
   "source": [
    "ak.to_pandas(jagged_array, how=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0595863",
   "metadata": {},
   "source": [
    "This is an inner merging: Some fields get lost"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9f6ced2",
   "metadata": {},
   "outputs": [],
   "source": [
    "ak.to_pandas(jagged_array, how='inner')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7456c8ce",
   "metadata": {},
   "source": [
    "This is an outer merging: We get some NANs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c92cf70",
   "metadata": {},
   "outputs": [],
   "source": [
    "nan_padded_df = ak.to_pandas(jagged_array, how='outer')\n",
    "nan_padded_df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "701a2241",
   "metadata": {},
   "source": [
    "This is a nice way to build a zero padded regular table of data (by calling .fillna(0)) that can then be used in simple feedforward DNN"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d6368047",
   "metadata": {},
   "outputs": [],
   "source": [
    "nan_padded_df.fillna(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b510e90b",
   "metadata": {},
   "source": [
    "Other options to deal with jagged data include DNN architectures which are more flexible with respect to their inputs, for example Graph Neural Networks. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa41c6b3",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "podas",
   "language": "python",
   "name": "podas"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
